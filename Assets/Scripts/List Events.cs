﻿
// Здесь мы добавляем имена для событий
public enum EventKey 
{
	DamageOnPlayer, // Событие урон по Плееру
	RegenHP,
	PlayerDestroy, // Событие уничтожение Плеера
	EnemyDestroyed,
	LearningPointsEnded,// Очки обучения закончились
	LearningPointsReset,// Сброс использованных очков обучения
	LevelCompleted, // Уровень завершен
	UpdateRagePoint, // Обновилось значение RagePoint
	StartGame, // Игра началась (была нажата кнопка удара)
    GameOver, // По этому событию раздвигается Земля и из недр Девственницы несут дыры и пивасик
    NextWave,
	DamageOnBoss,
    SwitchWeapon

};

// Так как я напсиал сисему событий, у который методы могут принимать только один аргумент, 
// то для передачи большего количества аргументов надо создавать класс с данными
public class EnemyDestroyedEventData {
	public int exp;
	public int coins;
	public int expWeapon = 1;

	public EnemyDestroyedEventData(int Exp, int Coins){
		exp = Exp;
		coins = Coins;
	}
}