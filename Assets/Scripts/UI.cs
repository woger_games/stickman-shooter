﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
   public  InputJoystick j1;
   public  InputJoystick j2;
    static UI instence;
     void Start()
    {
        instence = this;
    }
    float GetActiveJoystick()
    {
        if (j1.isUsed&& !j2.isUsed)
        {
            print("j1");
            return j1.getHorizontal;
        
        }
        else if (j2.isUsed&&!j1.isUsed)  
        {
            print("j2");
            return j2.getHorizontal;
            
        }
        else
        {
            return 0;
        }
    }

    public static float gethorizontal
    {
        get
        {
            return instence.GetActiveJoystick();
        }
    }

    private void Awake()
    {
        
    }

    public void SwitchWeapon()
    {
        
        MyEventSystem.TriggerEvent(EventKey.SwitchWeapon);
    }
}
