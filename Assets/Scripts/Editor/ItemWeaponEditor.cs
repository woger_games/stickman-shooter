﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Items.Weapon))]
[CanEditMultipleObjects]
public class ItemWeaponEditor : Editor {

	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();

		EditorGUILayout.BeginVertical ("box");
		GUILayout.Space (5);
		if(GUILayout.Button("СБРОСИТЬ И СОХРАНИТЬ")){
			Item i = (Item)target;
			i.Reset ();
		}
		if(GUILayout.Button("СОХРАНИТЬ")){
			Item i = (Item)target;
			i.SaveData ();
		}
		if(GUILayout.Button("ЗАГРУЗИТЬ")){
			Item i = (Item)target;
			i.LoadData ();
		}
		GUILayout.Space (5);
		EditorGUILayout.EndVertical ();
	}
	

}
