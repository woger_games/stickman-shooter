﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EffectsHelper : MonoBehaviour {

	static EffectsHelper instance;

	void Awake(){
		instance = this;
	}

	public static Coroutine Move(Image img, Vector3 target, float time){
		try {
			return instance.StartCoroutine (SmoothMovement (img, target, time));
		} catch (System.Exception e){
			return null;
		}
	}

	public static Coroutine SetTransparent(Image img, float start, float target, float time){
		return instance.StartCoroutine (SmoothChange (img, start, target, time));
	}

	static IEnumerator SmoothMovement(Image img, Vector3 target, float time){
		Vector3 velocity = Vector3.zero;
		if (img) {// Хуй знает что за ошибка, тупо затычка, появилась недавно
			while (Vector2.Distance (img.rectTransform.anchoredPosition, target) > 0.05f) {
				img.rectTransform.anchoredPosition = Vector3.SmoothDamp (img.rectTransform.anchoredPosition, target, ref velocity, time);
				yield return new WaitForEndOfFrame ();
			}
		}
	}

	public static IEnumerator SmoothChange(Image img,float start,float target, float time){
		img.color = new Color (img.color.r, img.color.g, img.color.b, start);
		float velocity = 0;
		while (Mathf.Approximately(img.color.a, target) == false) {
			yield return new WaitForEndOfFrame ();
			Color c = img.color;
			c.a = Mathf.SmoothDamp (c.a, target, ref velocity, time);
			img.color = c;
		}
	}

	public static void BreakEffect(Coroutine cor){
		if(cor != null) instance.StopCoroutine (cor);
	}


}
