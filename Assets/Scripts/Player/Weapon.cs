﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Anima2D;
namespace Items {

	[CreateAssetMenu(fileName = "Weapon", menuName = "Stickman Reaper/Weapon")]
	public class Weapon : Item {

		[Header("Свойства Оружия")]
		[Space(8)]
		public float damage;
        public float weaponReload;
      
        [Tooltip("темп стрельбы ")]
        public float shotReload = 0.2f;

        public AudioClip Sound;
        public GameObject Gun;
        public Anima2D.Pose pose;

        //public AudioClip WeaponSpellMainSound;
       
        //   [Range(1, 3)] public int evolutionLvl = 1;
        //   public AnimationClip WeaponSpellClip;
        //   [Tooltip("Требуется опыта до следующего уровня * level")]
        //public int expToNextLevel = 10;
        //public int countLvlToNextEvolution = 3;
        //[Header("Инфо")]
        //public float exp;
        // public int currentLvl = 1;
        // [Range(0, 59)] public int SpellColDown;
        // public float spelldamage;
        // public RuntimeAnimatorController[] runtimeAnimator = new RuntimeAnimatorController[3];
        //      public Sprite[] sprites;
        public override void Select ()
		{
			//base.Select(()=> SetWeapon (ShopManager.GetEquipment));
		}

		public override string GetProperty_1 ()
		{
			return "Damage:" + damage.ToString ();
		}

		void SetWeapon (Equipment equipment){
			//ShopManager.SetItem ( ()=> {
			//	equipment ().SetWeapon(this);
			//} );
		}

		//public void AddExp(float exp, Action<Items.Weapon> evolutionProcess){
		//	this.exp += exp;
		//	if (this.exp > (currentLvl * expToNextLevel)) {
		//		this.exp -= currentLvl * expToNextLevel;
		//		currentLvl++;
		//		if (currentLvl > (evolutionLvl * countLvlToNextEvolution)) {
		//			if (evolutionLvl < 3) {
		//				evolutionLvl++;
  //                    /*  damage += countLvlToNextEvolution;*///____________________
  //                      evolutionProcess (this);
		//			}
		//		}
		//	}
		//	SaveData ();
		//}


		public override void SaveData(){
			string saveData = string.Format (
              
                "{0};",unlocked.ToString ());
            //"{0};{1};{2};{3}",
            //currentLvl.ToString (),
            //exp.ToString (),
            //evolutionLvl.ToString ()

            PlayerPrefs.SetString (name, saveData);
			PlayerPrefs.Save ();
		}

		public override void LoadData(){
			if (!PlayerPrefs.HasKey (name)) {
				return;
			}
			string loadData = PlayerPrefs.GetString (name);
			string[] parameters = loadData.Split (new char[] { ';' }, System.StringSplitOptions.RemoveEmptyEntries);

			unlocked = bool.Parse (parameters [0]);
			//currentLvl = int.Parse (parameters [1]);
			//exp = int.Parse (parameters [2]);
			//evolutionLvl = int.Parse (parameters [3]);
		}


		public override void Reset(){
			base.Reset ();
			//currentLvl = 1;
			//exp = 0;
			//evolutionLvl = 1;
			SaveData ();
		}
			
	}
}
