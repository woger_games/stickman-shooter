﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Stickman Reaper/Equipment", fileName = "New Preset")]
public class EquipmentPreset : ScriptableObject {

	[Header("-= НАБОР ЭКИПИРОВКИ =-")]
	[Space(8)]
	public Items.Weapon weapon;



  //  public void Equip(Player player)
  //  {

  //player.currentWeapon.SetWeaponSprite(weapon.sprites[weapon.evolutionLvl - 1]);

  //      //EquipWeaponAnimator(weapon, player);
      
  //      GameObject weapSpell = weapon.Weaponspell;
  //      string WeaponSpellName = weapSpell.name;
  //      Transform WeaponSpellTransform = player.transform.Find("WeaponSpell").transform;
  //      Instantiate(weapSpell, WeaponSpellTransform);
  //      Transform wS = WeaponSpellTransform.GetChild(0);
  //      wS.name = WeaponSpellName;
  //      player.animatorOverrideController["Void"] = weapon.WeaponSpellClip;
  //      Debug.Log(wS.name);
  //      if (wS.GetComponent<Weapon>() != null)
  //      {
  //          wS.GetComponent<Weapon>().weaponDamage = weapon.spelldamage;
  //          Debug.Log("WeaponSpell Soset Huy");
  //      }

  //      if (wS.transform.childCount > 0)
  //      {
  //          FindChildHelper a = new FindChildHelper();

  //          Transform[] WeaponSpellArray = a.FindChild(wS);
  //          Debug.Log("WeaponSpellArray.Length" + WeaponSpellArray.Length);
  //          for (int i = 0; i < WeaponSpellArray.Length; i++)
  //          {
  //              if (WeaponSpellArray[i].GetComponent<Weapon>())
  //              {
  //                  WeaponSpellArray[i].GetComponent<Weapon>().weaponDamage = weapon.spelldamage;
  //              }
  //          }

  //      }


  //      Transform SpellTransform = player.transform.Find("Spells").transform;

  //      if (spell_1)
  //      {

  //          GameObject Spell1 = spell_1.spell;
  //          Instantiate(Spell1, SpellTransform);
  //          Transform s1 = SpellTransform.GetChild(0);
  //          s1.name = spell_1.spell.name;
  //          player.animatorOverrideController["Void 1"] = spell_1.spellClip;


  //          if (s1.GetComponent<Weapon>() != null)
  //          {
  //              s1.GetComponent<Weapon>().weaponDamage = spell_1.damage;

  //          }

  //          if (s1.transform.childCount > 0)
  //          {

  //              FindChildHelper a = new FindChildHelper();

  //              Transform[] SpellArray = a.FindChild(s1);

  //              for (int i = 0; i < SpellArray.Length; i++)
  //              {
  //                  if (SpellArray[i].GetComponent<Weapon>())
  //                  {
  //                      SpellArray[i].GetComponent<Weapon>().weaponDamage = spell_1.damage;
  //                  }

  //              }
  //          }



  //      }
  //      else
  //      {
  //          player.animatorOverrideController["Void 1"] = null;
  //      }

  //      if (spell_2)
  //      {
  //          GameObject Spell2 = spell_2.spell;
  //          Instantiate(Spell2, SpellTransform);
  //          Transform s2 = SpellTransform.GetChild(1);
  //          s2.name = spell_2.spell.name;
  //          player.animatorOverrideController["Void 2"] = spell_2.spellClip;

  //          if (s2.GetComponent<Weapon>() != null)
  //          {
  //              s2.GetComponent<Weapon>().weaponDamage = spell_2.damage;

  //          }

  //          if (s2.transform.childCount > 0)
  //          {
  //              FindChildHelper a = new FindChildHelper();

  //              Transform[] SpellArray = a.FindChild(s2);

  //              for (int i = 0; i < SpellArray.Length; i++)
  //              {
  //                  if (SpellArray[i].GetComponent<Weapon>())
  //                  {
  //                      SpellArray[i].GetComponent<Weapon>().weaponDamage = spell_2.damage;
  //                  }

  //              }
  //          }

  //      }
  //      else
  //      {
  //          player.animatorOverrideController["Void 2"] = null;
  //      }


  //  }//______________________________________________________________________________

  //  public static void EquipWeaponAnimator(Items.Weapon weapon, Player player)
  //  {
  //      Animator animator = player.GetComponent<Animator>();
  //      player.currentWeapon.weaponDamage = weapon.damage;

  //      animator.runtimeAnimatorController = weapon.runtimeAnimator[weapon.evolutionLvl - 1];
  //      player.currentWeapon.itemWeapon = weapon;

  //      player.animatorOverrideController.runtimeAnimatorController = animator.runtimeAnimatorController;
  //      player.animatorOverrideController.name = animator.runtimeAnimatorController.name;
  //      animator.runtimeAnimatorController = player.animatorOverrideController;

  //      if (animator.runtimeAnimatorController.name != "Custom")// Custom аниматор используется для тестов и как шаблон
  //      {
  //          player.animatorName = animator.runtimeAnimatorController.name;//присваиваем name текущего аниматора    
  //      }
  //  }

  //  public void SetWeapon(Items.Weapon weapon)
  //  {
  //      this.weapon = weapon;
  //      HolderEquippedItems.SetWeapon();
  //      SaveController.SaveEquipment(this);
  //  }//______________________________________________________________________________

  //  public void SetSpell(Items.Spell spell)
  //  {
  //      if (spell == spell_1)
  //      {
  //          spell_1 = null;
  //          HolderEquippedItems.SetSpell_1(null);
  //      }
  //      else if (spell == spell_2)
  //      {
  //          spell_2 = null;
  //          HolderEquippedItems.SetSpell_2(null);
  //      }
  //      else if (spell_1 == null)
  //      {
  //          spell_1 = spell;
  //          HolderEquippedItems.SetSpell_1(spell_1, true);
  //      }
  //      else if (spell_2 == null)
  //      {
  //          spell_2 = spell;
  //          HolderEquippedItems.SetSpell_2(spell_2, true);
  //      }
  //      SaveController.SaveEquipment(this);
  //  }//______________________________________________________________________________


  //  public void AddBottle(int countBuyedBottle)
  //  {
  //      bottle.count += countBuyedBottle;
  //      HolderEquippedItems.SetBottle(bottle);
  //  }//______________________________________________________________________________


}
