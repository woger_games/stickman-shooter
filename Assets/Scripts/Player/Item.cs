﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Item : ScriptableObject {

    public AudioClip[] SoundEffect;
	public string titleName;
    //public Currency currency;
    public int price;
    public int requiredLevel;
    public Sprite iconItem;


    [HideInInspector] public bool unlocked;

    public delegate EquipmentPreset Equipment();

    public virtual void OnEnable()
    {
        LoadData();
    }

    public virtual void Select()
    {
        // поведение общее для всех слотов при клике по ним, но пока такого нет
    }//________________________________________

    public virtual void Select(Action ActionSelect)
    {
        if (unlocked)
        {
            ActionSelect();
        }
        else
        {
            Debug.Log("SOSNI XYIEC");
        }
        //ShopManager.UpdateAllSlots();
    }//_________________________________________

    public virtual void Buy()
    {
        //switch (currency)
        //{
        //    //case Currency.Coins:
        //    //    {
        //    //        if (ShopManager.coins >= price)
        //    //        {
        //    //            unlocked = true;
        //    //            ShopManager.coins -= price;
        //    //            Select();
        //    //            SaveData();
        //    //        }
        //    //        break;
        //    //    }
        //    //case Currency.Fearters:
        //    //    {
        //    //        if (ShopManager.fearters >= price)
        //    //        {
        //    //            unlocked = true;
        //    //            ShopManager.fearters -= price;
        //    //            Select();
        //    //            SaveData();
        //    //        }
        //    //        break;
        //    //    }
        //}

        //ShopManager.UpdateViewSlotItem(this);
    }//_________________________________________


    public virtual string GetProperty_1()
    {
        return string.Empty;
    }

    public virtual void SaveData()
    {
        PlayerPrefs.SetString(name, unlocked.ToString());
        PlayerPrefs.Save();
    }

    public virtual void LoadData()
    {
        if (!PlayerPrefs.HasKey(name))
        {
            return;
        }
        unlocked = bool.Parse(PlayerPrefs.GetString(name));

    }

    public virtual void Reset()
    {
        PlayerPrefs.DeleteKey(name);
        unlocked = false;
        SaveData();
    }

}


