﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShot : MonoBehaviour
{
   
    //Время, через которое наша пуля
    //уйдет в пул объектов
    public float TimeToDisable;
  
    Rigidbody2D body;
    //Каждый раз, когда наша пуля активируется
    Vector3 startpos;
    private void Awake()
    {
       

        print("i Awake");
        startpos = transform.position;
        body = gameObject.GetComponent<Rigidbody2D>();
    }
    IEnumerator  Start()
    {
       
        yield return  null;
       
       
        
    }
    void OnEnable()
    {
       
        //Мы будем запускать таймер для того
        //Чтоб выключить её
        StartCoroutine(SetDisabled(TimeToDisable));

    }


    IEnumerator SetDisabled(float TimeToDisable)
    {
        
        //Данный скрип приостановит свое исполнение на 
        //TimeToDisable секунд, а потом продолжит работу
        yield return new WaitForSeconds(TimeToDisable);
      
        //Выключаем объект, он у нас останется в пуле объектов
        //до следующего использования
        gameObject.SetActive(false);
       
    }



   


    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Тут должна быть ваша обработка попадания
        //Вместо этого условия необходимо ваше, которое определит, что
        //в Collision находится именно тот объект, который вам нужен
        //if (collision == null)
        //{
        //    //Выключаем ожидание выключения чтоб в случае чего не создавать
        //    //несколько копий ожиданий
        //    StopCoroutine("SetDisabled");
        //    //Выключаем объект
        //    gameObject.SetActive(false);
        //    //Дальше весь тот код, который нужнен для вашей игры

        //}
    }




}
