﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InputJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

   public bool isUsed;

	Image arrowsImg;
	Image joystickImg;
	Vector2 inputVector;
	static InputJoystick instance;
	Coroutine executableEffect;//здесь хранится выполняемый над объектом эффект

	public  float getHorizontal {
        get
        {
            if (isUsed)
            {
                return inputVector.x;
            }
            else
            {
                return 0;
            }
			
		} 
	}

	void Start () {
		arrowsImg = GetComponent<Image> ();
		joystickImg = transform.GetChild (0).GetComponent<Image> ();
		instance = this;
	}

    void OnEnable()
    {
        inputVector = Vector2.zero;
        executableEffect = EffectsHelper.Move(joystickImg, Vector3.zero, 0.1f);
    }

    public virtual void OnDrag(PointerEventData ped){
		Vector2 pos;
		float sizeDeltaX = arrowsImg.rectTransform.rect.width * 0.3f;

		if (RectTransformUtility.ScreenPointToLocalPointInRectangle (
			arrowsImg.rectTransform,
			ped.position,
			ped.pressEventCamera,
			out pos)) {
			// создание вводных данных с джойстика
			pos.x = pos.x / sizeDeltaX;
			inputVector = new Vector2 (pos.x * 2, 0); //умножаем на 2 так как X имеет диапазон -0.5..0.5
			inputVector.x = Mathf.Clamp (inputVector.x, -1, 1);

			// Перемещение джойстика по панели стрелок
			joystickImg.rectTransform.anchoredPosition = new Vector2 (inputVector.x * (sizeDeltaX / 1.8f), 0);
		}														   
	}
		
	public virtual void OnPointerDown(PointerEventData ped){
        isUsed = true;
        OnDrag (ped);
		EffectsHelper.BreakEffect (executableEffect);
	}

	public virtual void OnPointerUp(PointerEventData ped){
        isUsed = false;
        inputVector = Vector2.zero;
		executableEffect = EffectsHelper.Move(joystickImg,Vector3.zero,0.1f);
	}
		
}
