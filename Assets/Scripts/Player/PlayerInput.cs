﻿using System.Collections;
using UnityEngine;
//using UnityStandardAssets.CrossPlatformInput;

[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour {

    
	Player player;// Reference to PlayerMovement script
    bool isJumping;


    void Awake() {
        player = GetComponent<Player>();
	}


	void Update ()
    {
        //If he is not jumping...
	    if (!isJumping)
        {
            //See if button is pressed...
            isJumping = Input.GetButtonDown("Jump");
      	}

 		float speed = 0;

        speed = MovingController();
        player.Move(speed);
  //      //Call movement function in PlayerMovement
  //if (!FinishingSystem.globalEpic) {// TODO
  //	player.Move (speed/*, isJumping/**/);
  //}
    }

    #region MyCode

    float speed = 0f;

    public float MovingController()
    {
		
		float speed = UI.gethorizontal;
		#if DEBUG
		if (Mathf.Approximately (Input.GetAxis ("Horizontal"), 0) == false) {
			speed = Input.GetAxis ("Horizontal");
		}
		#endif
		return speed;
    }
      
    public void Jumping() {  
		//player.Jump(MovingController());
	}
    
    #endregion -------------------------------------------

}
