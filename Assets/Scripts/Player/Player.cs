﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Anima2D;
using UnityEditor;
using UnityEditorInternal;
public class Player : MonoBehaviour
{
    
    
    [Header("--    Экипировка    --")]
    //[SerializeField]
  public  EquipmentPreset equipment;// Экипировка
    [Header("--    Настройки    --")]
    [SerializeField] float maxSpeed = 10f;
    [SerializeField] float jumpForce = 40f;
    [SerializeField] LayerMask whatIsGround;

    [Header("--    Поворот оружия под курсор(-30 30)    --")]
    public Transform zRotate; // объект для вращения по оси Z
     public float minAngle ;
    public float maxAngle ;
    

    private float angle;
    private int invert;
    private Vector3 mouse;

    [Header("--    ______    --")]
 
   
    Transform GroundCheck;
    const float k_GroundedRadius = .2f;
    bool grounded;
    [Header("--        --")]
    [SerializeField] bool FacingRight = true;
    Vector3 vecRight;
    Vector3 vecLeft;

    PlayerInput input;
    Rigidbody2D _Rigidbody2D;
    Animator animator;
    FireArms CurrentWeapon;


    //Наш пул объектов, который помогает
    //сохранить память при его использовании
    //Ведь активировать объект гораздо проще, 
    //чем его опять создавать
    public int bulletCount;
    public Transform gunPoint; // точка рождения
    public List<GameObject> BulletPool = new List<GameObject>();
    public float bulletSpead;
    public float shotReload = 0.2f;
    private float shoot_coldown;
   
    //
    private PoseManager PM; 
    public bool CanAttack
    {
        get
        {
            return shoot_coldown <= 0f;
        }
    }

  

    private void Awake()
    {

        Equip();

        vecRight = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        vecLeft = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        animator = GetComponent<Animator>();
        _Rigidbody2D = GetComponent<Rigidbody2D>();
        input = GetComponent<PlayerInput>();
        GroundCheck = transform.Find("GroundCheck");
        PM = GetComponent<PoseManager>();
    }

    void Equip()
    { Transform weaponPos = GameObject.Find("Weapon").transform;
        Instantiate(equipment.weapon.Gun, weaponPos);

        

      
        gunPoint = FindObjectOfType<FireArms>().gunPoint;
       
    }

    void LoadAmmo()
    {
        for (int i = 0; i < bulletCount; i++)
        {
        BulletPool.Add((GameObject)Instantiate(Resources.Load("Prefub/Bullet2"), gunPoint.position, gunPoint.rotation));
            BulletPool[i].SetActive(false);
        }
        
    }
    
    private void Start()
    {
        LoadAmmo();
        shoot_coldown = 0f;
        if (!FacingRight) invert = -1; else invert = 1;
    }
      
    void BulletFlight( GameObject bullet)
    {
        float randBS = bulletSpead + Random.Range(1, bulletSpead/2);
            Rigidbody2D body = bullet.GetComponent<Rigidbody2D>();
            body.position = gunPoint.position;
        body.transform.rotation = Quaternion.identity;
        
        body.velocity = transform.TransformDirection(gunPoint.right * randBS);

            if (!FacingRight) { body.velocity = -body.velocity;  }

        body.transform.localScale = gunPoint.lossyScale;
               body.transform.right = gunPoint.right;


    }
       
    public void Fire()
    {
        
            //Показывает, нашли ли мы выключенный объект в нашем массиве
            bool freeBullet = false;
            //Теперь необходимо проверить, есть ли выключенный объект в нашем пуле
            for (int i = 0; i < BulletPool.Count; i++)
            {
                //Смотрим, активен ли объект в игровом пространстве
                if (!BulletPool[i].activeInHierarchy)
                {
                    //Если объект не активен
                    //То мы задаем ему все нужные параметры
                    //Позицию
                    BulletPool[i].transform.position = gunPoint.position;
                //Поворот
                BulletPool[i].transform.rotation = gunPoint.rotation;

                //И опять его включаем
                BulletPool[i].SetActive(true);
                    //Ставим объект найденным, чтоб опять не создавать лишний
                   
                    freeBullet = true;
                     BulletFlight(BulletPool[i]);
                break;
                }
            }
            //если свободный объект не был найден, то нужно создать еще один
            if (!freeBullet)
            {
            //Создаем объект с нужными значениями и заносим его в пул
            //BulletPool.Add((GameObject)Instantiate(Resources.Load("Prefub/Bullet2"), gunPoint.position, gunPoint.rotation));
            Debug.Log(" Пуля проебалась");
            }
            
        
        
    }

    void Update()
    {
        
            shoot_coldown -= Time.deltaTime;

        

    
     
    }
 

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0) )
        {


            if (!EventSystem.current.IsPointerOverGameObject())
            {
            if (zRotate) SetRotationWeapon();
            if (mouse.x < transform.position.x && FacingRight) Flip();
            else if (mouse.x > transform.position.x && !FacingRight) Flip();

            if (CanAttack)
            {
                Fire();
                shoot_coldown = shotReload + Random.Range(0.01f, 0.05f);
            }
            }
            else
            {
            Debug.Log("Clicked on the UI");
             }

        }
        else
        {

        }
         
        
       

        grounded = false;
        Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, k_GroundedRadius, whatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject && colliders[i].gameObject.tag != "weapon")
                grounded = true;
        }

        animator.SetBool("Ground", grounded);
        animator.SetFloat("vSpeed", _Rigidbody2D.velocity.y);

    if (transform.position.y < -8f)////__________TO DO
        {
            float x = transform.position.x;
            float z = transform.position.z;

            transform.position = new Vector3(x, -5, z);
            print("Игрок провалился!!!!!!!!!!!");
        }
    }

    public void Move(float speed/*, bool jump/**/)
    {
        
        if (grounded )
        {
                animator.SetFloat("Speed", Mathf.Abs(speed));
                transform.Translate(new Vector3(speed * maxSpeed * Time.deltaTime, 0, 0), Space.World);
         
          
        }
    }

    public void Flip()
    {
        
        FacingRight = !FacingRight;
        invert *= -1;
        if (FacingRight)
        {
            transform.localScale = vecLeft;
            
        }
        else
        {
            transform.localScale = vecRight;
           
        }

        print(gunPoint.lossyScale.x);
    }

    //public void Jump(float speedAxisX)
    //{

    //    if (grounded && animator.GetBool("Ground"))
    //    {
    //        grounded = false;
    //        animator.SetBool("Ground", false);
    //        float forceY = jumpForce * GetComponent<Rigidbody2D>().mass;
    //        _Rigidbody2D.AddForce(new Vector2(speedAxisX, forceY), ForceMode2D.Impulse);
    //    }
    //}//________________________________________________________________________


    public void Jump()
    {
        float forceY = jumpForce * GetComponent<Rigidbody2D>().mass;
        _Rigidbody2D.AddForce(new Vector2(0, forceY), ForceMode2D.Impulse);
    }

    //void FlipWeapon() // отражение по горизонтали
    //{
    //    facingRightWeapon = !facingRightWeapon;
    //    Vector3 theScale = zRotate.localScale;

    //    zRotate.localScale = -theScale;
    //}

  
    void SetRotationWeapon()//Поворот под курсор
    {

        Vector3 mousePosMain = Input.mousePosition;
        mousePosMain.z = Camera.main.transform.position.z;
        mouse = Camera.main.ScreenToWorldPoint(mousePosMain);
        Vector3 lookPos = mouse - zRotate.position;
        angle = Mathf.Atan2(lookPos.y, lookPos.x * invert) * Mathf.Rad2Deg;
        angle = Mathf.Clamp(angle * invert, minAngle, maxAngle);

        zRotate.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        

    }


    void SwitchWeapon()//
    {
        print("Cmena puxu");
    }

    void OnEnable()
    {
        MyEventSystem.StartListening(EventKey.SwitchWeapon, new UnityEngine.Events.UnityAction(SwitchWeapon));
    }


    }
