﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MyEventSystem : MonoBehaviour {
	//--------------------------------------------------------------------------
	// ЕСЛИ НУЖНЫ КОММЕНТЫ К ЭТОМУ ЗЛО-ЕБУЧЕМУ КОДУ, ТО НАПИШИ МНЕ Я ВСЁ ОПИШУ -
	//--------------------------------------------------------------------------
	public Dictionary<EventKey, UnityEvent> events;

	public Dictionary<EventKey, List<object>> eventsWithParam;

	static MyEventSystem eventSystem;

	public static MyEventSystem instance {
		get {
			if (!eventSystem) {
				eventSystem = FindObjectOfType<MyEventSystem> ();

				if (!eventSystem) {
					Debug.LogError ("На сцене должен быть один GameObject с этим скриптом");
				} else {
					eventSystem.Init (); 
				}
			}
			return eventSystem;
		}
	}

	void Init () {
		if (events == null) {
			events = new Dictionary<EventKey, UnityEvent> ();
		}
		if (eventsWithParam == null) {
			eventsWithParam = new Dictionary<EventKey, List<object>> ();
		}
	}

	[System.Serializable]
	public class GenericUnityEvent<T>: UnityEvent<T> {}

	public static void StartListening<T>(EventKey eventName, UnityAction<T> listener) {

		GenericUnityEvent<T> thisEvent = null;
		List<object> eventHandler = null;

		if (instance.eventsWithParam.TryGetValue (eventName, out eventHandler)) {

			foreach (object obj in eventHandler) {
				if (obj.GetType ().Equals (typeof(GenericUnityEvent<T>))) {
					thisEvent = (GenericUnityEvent<T>) obj;
					thisEvent.AddListener (listener);
					return;
				}
			}

			thisEvent = new GenericUnityEvent<T> ();
			thisEvent.AddListener (listener);
			eventHandler.Add (thisEvent);

		} else {
			thisEvent = new GenericUnityEvent<T> ();
			thisEvent.AddListener (listener);
			List<object> listEvent = new List<object> ();
			listEvent.Add (thisEvent);
			instance.eventsWithParam.Add (eventName, listEvent);/**/
		}
	}

	public static void StartListening (EventKey eventName, UnityAction listener) {

		UnityEvent thisEvent = null;
		if (instance.events.TryGetValue (eventName, out thisEvent)) {
			thisEvent.AddListener (listener);
		} else {
			thisEvent = new UnityEvent ();
			thisEvent.AddListener (listener);
			instance.events.Add (eventName, thisEvent);
		}
	}

	public static void StopListening<T>(EventKey eventName, UnityAction<T> listener){
		if (eventSystem == null) return;

		List<object> eventsHandler = null;
		if (instance.eventsWithParam.TryGetValue (eventName, out eventsHandler)) {
			foreach (object obj in eventsHandler) {
				if (obj.GetType ().Equals (typeof(GenericUnityEvent<T>))) {
					GenericUnityEvent<T> e = (GenericUnityEvent<T>)obj;
					e.RemoveListener (listener);
				}
			}
		}
	}

	public static void StopListening (EventKey eventName, UnityAction listener) {

		if (eventSystem == null) return;
		UnityEvent thisEvent = null;
		if (instance.events.TryGetValue (eventName, out thisEvent)) {
			thisEvent.RemoveListener (listener);
		}
	}

	public static void TriggerEvent (EventKey eventName) {
		
		UnityEvent thisEvent = null;
		if (instance.events.TryGetValue (eventName, out thisEvent)) {
			thisEvent.Invoke ();
		} else {
			Debug.Log ("Ну ты мудила, вызвал событие, а подписчиков на него нет...");
		}
	}

	public static void TriggerEvent<T> (EventKey eventName, T arg) {
		List<object> eventHandler = null;

		if (instance.eventsWithParam.TryGetValue (eventName, out eventHandler)) {
			foreach (object obj in eventHandler) {
				if (obj.GetType ().Equals (typeof(GenericUnityEvent<T>))) {
					GenericUnityEvent<T> e = (GenericUnityEvent<T>)obj;
					e.Invoke (arg);
				}
			}
		} else {
			Debug.Log ("Ну ты еблан, нет же подписчиков на событие - " + eventName.ToString ());
		}
	}
}



