﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CamerasController : MonoBehaviour
{

    [SerializeField] CinemachineVirtualCameraBase m_Vcam1Group;
    [SerializeField] CinemachineVirtualCameraBase m_Vcam1GroupNoise;

    static GameObject targetGroup;
    static CinemachineTargetGroup.Target[] targets;
    static CinemachineVirtualCameraBase vcam1;
    static CinemachineVirtualCameraBase vcamNoise;
    static CinemachineVirtualCameraBase vcam1Group;
    static CinemachineVirtualCameraBase vcam1GroupNoise;
    static Player player;

    static CamerasController instance;
    static GameObject lastTarget;


    static CinemachineBrain cinemachineBrain;
    // Use this for initialization
    void Start()
    {
        vcam1Group = m_Vcam1Group;
        vcam1GroupNoise = m_Vcam1GroupNoise;
        player = FindObjectOfType<Player>();
        targetGroup = GameObject.Find("Target Group");
        vcam1 = GameObject.Find("CM vcam1").GetComponent<CinemachineVirtualCameraBase>();
        vcamNoise = GameObject.Find("CM vcam1 Noise").GetComponent<CinemachineVirtualCameraBase>();
        instance = this;
        cinemachineBrain = FindObjectOfType<CinemachineBrain>();
        cinemachineBrain.m_DefaultBlend.m_Time = 1.1f;
        FitCameraStack();
    }

    void Update()
    {

    }


    public static void SwitchToGroupTarget(GameObject secondTarget)
    {

        lastTarget = secondTarget;

        targets = targetGroup.GetComponent<CinemachineTargetGroup>().m_Targets;
        targets = new CinemachineTargetGroup.Target[2];
        Transform firstTarget = player.transform;
        targets[0].target = player.transform.Find("Bones").Find("Hip");
        targets[0].weight = 1;
        targets[0].radius = 5.5f;
        targets[1].target = secondTarget.transform;
        targets[1].weight = 0.7f;
        targets[1].radius = 4.1f;
        targetGroup.GetComponent<CinemachineTargetGroup>().m_Targets = targets;
        vcam1Group.Priority = 10;
        vcam1.Priority = 9;
    }

    public static void SwitchToPlayerTarget()
    {
        vcam1.Priority = 10;
        vcam1Group.Priority = 9;
        vcam1GroupNoise.Priority = 9;
    }

    public static void SwitchToGroupTargetNoise()
    {
        vcam1GroupNoise.Priority = 11;
        vcam1Group.Priority = 9;
        vcam1.Priority = 9;
    }

    public static void Shake(float duration = 1.5f)
    {
        if (vcam1Group.Priority > 9)
        {
            instance.StartCoroutine(instance.StartShake(duration));
            //print ("klnlnln");
        }
    }

    IEnumerator StartShake(float duration)
    {
        SwitchToGroupTargetNoise();

        //cinemachineBrain.m_DefaultBlend.m_Time = 0.5f;
        yield return new WaitForSeconds(duration);
        vcam1GroupNoise.Priority = 9;
        vcam1Group.Priority = 10;
        //cinemachineBrain.m_DefaultBlend.m_Time = 1.5f;
    }

    void FitCameraStack()
    {
        Collider2D col = GameObject.Find("Cam Collider").GetComponent<Collider2D>();

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).GetComponent<CinemachineVirtualCamera>())
            {
                if (transform.GetChild(i).GetComponent<CinemachineVirtualCamera>().Follow == null)
                {
                    transform.GetChild(i).GetComponent<CinemachineVirtualCamera>().Follow = player.transform;
                }
            }

            if (transform.GetChild(i).GetComponent<CinemachineConfiner>())
            {
                transform.GetChild(i).GetComponent<CinemachineConfiner>().m_BoundingShape2D = col;
            }
        }
    }
}
